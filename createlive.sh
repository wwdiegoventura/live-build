#!/bin/bash
# REFERENCIAS:
# https://manpages.debian.org/stretch/live-build/lb_config.1.fr.html
# https://live-team.pages.debian.net/live-manual/html/live-manual/customizing-package-installation.en.html
# https://wiki.ubuntu.com/S390X/InstallationGuide/AutomatedInstallsWithPreseed?action=AttachFile&do=get&target=preseed_kvm.cfg
# https://www.vivaolinux.com.br/artigo/Criando-um-LiveCD-a-partir-de-uma-instalacao-do-Debian-Lenny
#https://framagit.org/dflinux/DFiso/
#
# Desenvolvido para DuZeru 4 Edição 0 DZ 4.0
# OBS: Debian 10 utiliza o padrão su - para acessar como root
# Autor: Cláudio A. Silva

#apt-get install git live-build live-manual live-config schroot -y

#git clone https://gitlab.com/duzeru/live-build.git

# CRIAR O DIRETORIO NECESSARIO PARA GERACAO DA LIVE
mkdir ~/live
cd ~/live
lb clean
lb config
echo -e '\033[01;32m Criado o diretório e preparado Skel da live \033[00;37m'
sleep 3

# INSERIR ARQUIVOS ESSENCIAIS
cd ~/live-build
cp -r ./archives/* ~/live/config/archives/
cp -r ./package-lists/* ~/live/config/package-lists/
cp -r ./includes.binary/* ~/live/config/includes.binary/
cp -r ./includes.chroot/* ~/live/config/includes.chroot/
#cp -r preferences config/apt/
#cp -r isolinux config/includes.binary/
#cp -r ../live-build/config/hooks/* ./config/hooks/ && \
#cp -r ../live-build/config/includes.installer/* ./config/includes.installer/
echo -e '\033[01;32m Copiado todos os diretorios da live com sucesso\033[00;37m'
sleep 5

cd ~/live

lb config \
--mode debian \
--distribution buster \
--parent-distribution buster \
--apt-indices false \
--backports true \
--image-name dz4 \
--iso-volume "duzerulinux" \
--iso-application "duzeru" \
--iso-preparer "duzeru" \
--iso-publisher "duzeru" \
--archive-areas "main contrib non-free" \
--apt-source-archives false \
--bootappend-live "boot=live components hostname=duzeru username=dz" \
--updates true \
--backports true \
--memtest none \
--debian-installer-gui false \
--debian-installer live \
--iso-publisher DuZeru \
--source false
#--mirror-chroot http://packages.duzeru.org \
#--mirror-binary http://packages.duzeru.org \
#--interactive shell
# --architectures i386 \
# --linux-flavours 686-pae \
#--mirror-binary http://packages.duzeru.org \
#--mirror-binary-security http://mirror/debian-security/ \

echo -e '\033[01;32m Pronto para gerar a imagem \033[00;37m'
sleep 5

lb build

#PODEMOS MANIPULAR A INSTALAÇÃO E O INSTALADOR DO DEBIAN
#ARQUIVO:

#vim config/includes.installer/preseed.cfg

# Não crie uma conta de usuário normal
#d-i passwd/make-user boolean false

#Atualizar pacotes instalados
#d-i pkgsel/upgrade select full-upgrade

# Desabilitar popularidade de pacotes
#popularity-contest popularity-contest/participate boolean false

#GRUB se instalar automaticamente caso não exista outro SO
#d-i grub-installer/only_debian boolean true

# Desativar repositórios de fontesdeb-src
#d-i apt-setup/enable-source-repositories boolean false

# Localização
#d-i debian-installer/language string en
#d-i debian-installer/country string US
#d-i debian-installer/locale string en_US.UTF-8 

# Timezone
#d-i time/zone string US/Eastern

# User
#d-i passwd/root-password password duzeru
#d-i passwd/root-password-again password duzeru

# Repositories
# Disable security, volatile and backports
#d-i apt-setup/services-select multiselect
# Enable contrib and non-free
#d-i apt-setup/non-free boolean true
#d-i apt-setup/contrib boolean true
# Disable source repositories too
#d-i apt-setup/enable-source-repositories boolean false
# Disable CDROM entries after install
#d-i apt-setup/disable-cdrom-entries boolean true
#d-i apt-setup/use_mirror boolean true
